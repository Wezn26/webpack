'use strict';

document.getElementById('loginButton').onclick = function () {

  require.ensure(['./login'], function (require) {
    const login = require('./login');

    login();
  }, 'auth');
};

document.getElementById('logoutButton').onclick = function () {

  require.ensure(['./logout'], function (require) {
    const logout = require('./logout');

    logout();
  }, 'auth');
};
