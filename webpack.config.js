const NODE_ENV = process.env.NODE_ENV || 'development';

const path = require('path');
const webpack = require('webpack');


module.exports = {
  mode: 'development',
  context: path.resolve(__dirname, 'app'),

  entry: {
    app: './app'
  },
  output: {
    path: path.resolve(__dirname, 'dist/js'),
    publicPath: '/js/',
    filename: '[name].js'
    //library: '[name]'
  },
  watch: NODE_ENV == 'development',

  watchOptions: {
    aggregateTimeout: 500
  },

  //devtool: NODE_ENV == 'development' ? 'source-map' : null,

  optimization: {
    noEmitOnErrors: true,
    splitChunks: {

         cacheGroups: {
         common: {
           name: 'common',
           minChunks: 1,
           chunks: "all"
         }
       }
    }
  },

  plugins: [
      new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV),
      LANG: JSON.stringify('ru')
    })
  ],

  resolve: {
    modules: ['node_modules'],
    extensions: [ '.js', '.json' ]
  },

  resolveLoader: {
    modules: [ 'node_modules' ],
    extensions: [ '.js', '.json' ],
    mainFields: [ 'loader', 'main' ]
  },

  module: {

    rules: [{
      test: /\.js$/,
      use: {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env'],
        plugins: ['@babel/plugin-transform-runtime']
      }
    }
    }]

  }

};

// if (NODE_ENV == 'production') {
//   module.exports.plugins.push(
//     new webpack.optimize.UglifyJsPlugin({
//       compress: {
//         warnings: false,
//         drop_console: true,
//         unsafe: true
//       }
//     })
//   );
// }
